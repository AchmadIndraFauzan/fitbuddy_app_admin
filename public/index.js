$('.progress').show();

var db = firebase.database();

var userRef = db.ref().child('user');
userRef.on("child_added", snap => {
    var email = snap.child("email").val();
    var firstname = snap.child("firstname").val();
    var lastname = snap.child("lastname").val();
    var password = snap.child("password").val();
    var phone_number = snap.child("phone_number").val();
    var username = snap.child("username").val();

    $('#user_data').append("<tr><td>"+ email +"</td><td>"+ firstname +"</td>"+
                            "<td>"+ lastname +"</td><td>"+ username +"</td>"+
                            "<td>"+ password +"</td><td>"+ phone_number +"</td></tr>");
    $('.progress').hide();
});

var fitfoodRef = db.ref().child("fit-food");
fitfoodRef.on("child_added", snap => {
    var deskripsi = snap.child("deskripsi").val();
    var harga = snap.child("harga").val();
    var imgUrl = snap.child("imgUrl").val();
    var name = snap.child("name").val();
    var stok = snap.child("stok").val();

    $('#fitfood_data')
    .append("<div class='col s12 m3'>"+
    "<div class='card'>"+
    "<div class='card-image'>"+
    "<img src='"+ imgUrl +"' style='height: 200px; object-fit: cover;'>"+
    "<span class='card-title' style='background: rgba(0, 0, 0, 0.39); width: 100%;'>"+ name +"</span>"+     
    "</div>"+        
    "<div class='card-content'>"+      
    "<p style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden;'>"+ deskripsi +"</p>"+ 
    "</div>"+        
    "<div class='card-action'>"+
    "<p> Stok: "+ stok +" Harga: "+ harga +"</p>"+
    "</div></div>");
    $('.progress').hide();

}); 

var exerciseRef = db.ref().child("exercise");
exerciseRef.on("child_added", snap => {
    var date = snap.child("date").val();
    var thumbnailUrl = snap.child("thumbnailUrl").val();
    var title = snap.child("title").val();
    var videoUrl = snap.child("videoUrl").val();

    $('#exercise_data')
    .append("<div class='col s12 m3'>"+
    "<a href='"+videoUrl+"' style='color: black;' target='_blank'><div class='card' style='height: 250px;'>"+
    "<div class='card-image'>"+
    "<img src='"+ thumbnailUrl +"'>"+
    "</div>"+        
    "<p style='font-size: 16px; padding-left: 20px;'>"+ title +"</p>"+
    // "<p style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden;'>"+ date +"</p>"+      
    "</div></a>");
    $('.progress').hide();

}); 

var fithealthRef = db.ref().child("fit-health");
fithealthRef.on("child_added", snap => {
    var date = snap.child("date").val();
    var artikel = snap.child("artikel").val();
    var title = snap.child("title").val();
    var imgUrl = snap.child("imgUrl").val();

    $('#fithealth_data')
    .append("<div class='col s12 m5'>"+
    
    "<div class='card horizontal' style='height: 130px;'>"+
    "<div class='card-image'>"+
    "<img src='"+ imgUrl +"' style='height: 130px; width: 180px; object-fit: cover;'>"+
    "</div>"+    
    "<div class='card-stacked'>"+
    "<div class='card-content'>"+       
    "<p style='font-size: 14px;'>"+ title +"</p>"+
    "<p style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden;'>"+ date +"</p>"+   
    // "<a class='waves-effect waves-light btn modal-trigger right' href='#modal1'>See Detail</a>"+
    "</div></div>"
    // "<div id='modal1' class='modal'>"+
    // "<div class='modal-content'>"+
    // "<h4>Modal Header</h4>"+
    // "<p>A bunch of text</p>"+
    // "</div>"+
    // "<div class='modal-footer'>"+
    // "<a href='#!' class='modal-close waves-effect waves-green btn-flat'>Agree</a>"+
    // "</div></div>"
    );
    $('.progress').hide();

}); 

var transactionRef = db.ref().child("transfers");
transactionRef.on("child_added", snap => {

    var uid = snap.child("uid").val();
    var status = snap.child("status").val();
    var transaction_code = snap.child("transaction_code").val();
    var imgUrl = snap.child("imgUrl").val();

    $('#transaction_data').append("<tr><td>"+ transaction_code +"</td><td>"+ uid +"</td>"+
    "<td class ='status_td'>"+ status +"</td><td><a href='"+ imgUrl +"' target='_blank'> view image </a></td>"+
    "<td class='action_td'><button class='waves-effect waves-light btn approve' style='margin-right: 20px;'>Approve</button><button id='' class='unapprove waves-effect waves-light btn')'>Unapprove</button></td></tr>");
    $('.progress').hide();

    $('.approve').click(function(){

        var updates = {};
        updates['/transfers/'+transaction_code+'/status'] = 'Paid';
        updates['/payments/'+transaction_code+'/status'] = 'Paid';

        firebase.database().ref().update(updates);
    });

    $('.unapprove').click(function(){

        var updates = {};
        updates['/transfers/'+transaction_code+'/status'] = 'Unapprove By Admin';
        updates['/payments/'+transaction_code+'/status'] = 'Unapprove By Admin';

        firebase.database().ref().update(updates);
    });
 
}); 